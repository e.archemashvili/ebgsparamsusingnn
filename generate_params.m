function [r_0,r_1, r_2, r_3, r_4, x_0, x_1, x_2, x_3, x_4, y_0, y_1, y_2, y_3, y_4, diel] = generate_params(Max_radius, Min_radius, Max_x, Min_x, Max_y, Min_y, Min_diel, Max_diel)
    r_0 = Min_radius + rand(1) * (Max_radius - Min_radius);
    r_1 = Min_radius + rand(1) * (Max_radius - Min_radius);
    r_2 = Min_radius + rand(1) * (Max_radius - Min_radius);
    r_3 = Min_radius + rand(1) * (Max_radius - Min_radius);
    r_4 = Min_radius + rand(1) * (Max_radius - Min_radius);

    x_0 = Min_x + rand(1) * (Max_x - Min_x);
    x_1 = Min_x + rand(1) * (Max_x - Min_x);
    x_2 = Min_x + rand(1) * (Max_x - Min_x);
    x_3 = Min_x + rand(1) * (Max_x - Min_x);
    x_4 = Min_x + rand(1) * (Max_x - Min_x);

    y_0 = Min_y + rand(1) * (Max_y - Min_y);
    y_1 = Min_y + rand(1) * (Max_y - Min_y);
    y_2 = Min_y + rand(1) * (Max_y - Min_y);
    y_3 = Min_y + rand(1) * (Max_y - Min_y);
    y_4 = Min_y + rand(1) * (Max_y - Min_y);
    
    diel = Min_diel + rand(1) * (Max_diel - Min_diel);
end

