function [res] = get_vector_string(vec)
    N = size(vec, 2);
    res = "";
    for p=1:N
        res = res + vec(p) + '; ';
    end
end

