function OBJ = collect_data()
% clear; close all;
% vars = rand(1,5)*0+1;

%% Variable bounds

% I usually set up the optimizer to pass in a variable between [0,1]. These
% variables then need to be scaled to realistic bounds. For example, the
% radius of the circle might between

% Lattice spacing
dx = 1.96;
cr = 0.18*dx;

epsilon = 0.005;

Max_radius = 0.6;
Min_radius = 0.01;

Max_x = 20;
Min_x = 5;

Max_y = 0.5;
Min_y = -0.5;

Min_diel = 2;
Max_diel = 16;



[f_r_0,f_r_1, f_r_2, f_r_3, f_r_4, f_x_0, f_x_1, f_x_2, f_x_3, f_x_4, f_y_0, f_y_1, f_y_2, f_y_3, f_y_4, diel] =  generate_params_while(Max_radius, Min_radius, Max_x, Min_x, Max_y, Min_y, Min_diel, Max_diel);
f_r_0
f_r_1
f_r_2
f_y_4

    

%% Open CST model 

% Open CST 
cst = actxserver('CSTStudio.Application');

% Create a new empty window
mws = invoke(cst, 'NewMWS');

% Check to see if the optimized model exist from a previous run. If so,
% delete it and the corresponding files
if exist('Opt.cst','file')
    delete('Opt.cst')
end
if exist('Opt','Dir')
    try
    catch
        pause(10)
        rmdir('Opt','s')
    end
end
        rmdir('Opt','s')
% Copy model to a new model ('Opt.cst') that will be modified 
copyfile('dielectric.cst','Opt.cst')

% Open Desired file
invoke(mws,'OpenFile', [pwd,filesep,'Opt.cst']);

%% Update parameter files

% Update the parameter values
mws.invoke('StoreDoubleParameter','f_r_0',f_r_0);
mws.invoke('StoreDoubleParameter','f_r_1',f_r_1);
mws.invoke('StoreDoubleParameter','f_r_2',f_r_2);
mws.invoke('StoreDoubleParameter','f_r_3',f_r_3);
mws.invoke('StoreDoubleParameter','f_r_4',f_r_4);
mws.invoke('StoreDoubleParameter','f_x_0',f_x_0);
mws.invoke('StoreDoubleParameter','f_x_1',f_x_1);
mws.invoke('StoreDoubleParameter','f_x_2',f_x_2);
mws.invoke('StoreDoubleParameter','f_x_3',f_x_3);
mws.invoke('StoreDoubleParameter','f_x_4',f_x_4);
mws.invoke('StoreDoubleParameter','f_y_0',f_y_0);
mws.invoke('StoreDoubleParameter','f_y_1',f_y_1);
mws.invoke('StoreDoubleParameter','f_y_2',f_y_2);
mws.invoke('StoreDoubleParameter','f_y_3',f_y_3);
mws.invoke('StoreDoubleParameter','f_y_4',f_y_4);
mws.invoke('StoreDoubleParameter','diel',diel);


% Update the model before solving
mws.invoke('RebuildOnParametricChange',false,false);

%% Run CST simulation

% Time domain solver 'Solver'
% Freq domain 'FDSolver'
% Eigenmode 'EigenmodeSolver'
solver = invoke(mws,'FDSolver');

% Start solver
invoke(solver, 'start');

% save file
% invoke(mws, 'save')

% close file
% invoke(cst,'quit');

%% Retrieve S-parameters

resultTree = mws.invoke('Resulttree');

sparamname = {'S1,1';'S2,1';'S1,2';'S2,2'};
sparam = [];
for ii = 1:4
result1D = resultTree.invoke('GetResultFromTreeItem',['1D Results\S-Parameters\',sparamname{ii}],'3D:RunID:0');
freq = result1D.invoke('GetArray','x');
s_real = result1D.invoke('GetArray','yre');
s_im = result1D.invoke('GetArray','yim');
sparam(ii,:) = s_real + 1i*s_im;
end

% Close CST model
invoke(mws,'quit');

% Plot the s-parameters
figure(1); clf;
hold on
s_params_vectors = [];
params_vector = [f_r_0,f_r_1, f_r_2, f_r_3, f_r_4, f_x_0, f_x_1, f_x_2, f_x_3, f_x_4, f_y_0, f_y_1, f_y_2, f_y_3, f_y_4, diel];
for ii = 1:4
    plot(freq,20*log10(abs(sparam(ii,:))),'DisplayName',sparamname{ii});
    s_params_vectors = [s_params_vectors get_vector_string(sparam(ii, :))];
end

T = array2table([params_vector s_params_vectors]);
T.Properties.VariableNames = {'f_r_0','f_r_1', 'f_r_2', 'f_r_3', 'f_r_4', 'f_x_0', 'f_x_1', 'f_x_2', 'f_x_3', 'f_x_4', 'f_y_0', 'f_y_1', 'f_y_2', 'f_y_3', 'f_y_4', 'diel', 'S_11', 'S_21', 'S_12', 'S_22'};
writetable(T, 'data.csv', 'WriteMode', 'Append')
xlabel('Frequency (GHz)')
ylabel('S-parameter (dB)')
legend('Location','SouthWest')


pause(5);





