function OBJ = cost(vars)
% clear; close all;
% vars = rand(1,5)*0+1;

%% Variable bounds

% I usually set up the optimizer to pass in a variable between [0,1]. These
% variables then need to be scaled to realistic bounds. For example, the
% radius of the circle might between

% Lattice spacing
dx = 1.96;
cr = 0.18*dx;

epsilon = 0.005;

Max_radius = 0.6
Min_radius = 0.01

Max_x = 20
Min_x = 5

Max_y = 0.7
Min_y = -0.7

f_r_0 = Min_radius + rand(1) * (Max_radius - Min_radius);
f_r_1 = Min_radius + rand(1) * (Max_radius - Min_radius);
f_r_2 = Min_radius + rand(1) * (Max_radius - Min_radius);
f_r_3 = Min_radius + rand(1) * (Max_radius - Min_radius);
f_r_4 = Min_radius + rand(1) * (Max_radius - Min_radius);


f_x_0 = Min_x + rand(1) * (Max_x - Min_x);
f_x_1 = Min_x + rand(1) * (Max_x - Min_x);
f_x_2 = Min_x + rand(1) * (Max_x - Min_x);
f_x_3 = Min_x + rand(1) * (Max_x - Min_x);
f_x_4 = Min_x + rand(1) * (Max_x - Min_x);




f_y_0 = Min_y + rand(1) * (Max_y - Min_y);
f_y_1 = Min_y + rand(1) * (Max_y - Min_y);
f_y_2 = Min_y + rand(1) * (Max_y - Min_y);
f_y_3 = Min_y + rand(1) * (Max_y - Min_y);
f_y_4 = Min_y + rand(1) * (Max_y - Min_y);




f_r_0,f_r_1, f_r_2, f_r_3, f_r_4, f_x_0, f_x_1, f_x_2, f_x_3, f_x_4, f_y_0, f_y_1, f_y_2, f_y_3, f_y_4 =  generate_params(Max_radius, Min_radius, Max_x, Min_x, Max_y, Min_y);
f_r_0
f_r_1
f_r_2

    

%% Open CST model 

% Open CST 
cst = actxserver('CSTStudio.Application');

% Create a new empty window
mws = invoke(cst, 'NewMWS');

% Check to see if the optimized model exist from a previous run. If so,
% delete it and the corresponding files
if exist('Opt.cst','file')
    delete('Opt.cst')
end
if exist('Opt','Dir')
    try
    catch
        pause(10)
        rmdir('Opt','s')
    end
end
        rmdir('Opt','s')
% Copy model to a new model ('Opt.cst') that will be modified 
copyfile('dielectric.cst','Opt.cst')

% Open Desired file
invoke(mws,'OpenFile', [pwd,filesep,'Opt.cst']);

%% Update parameter files

% Update the parameter values
mws.invoke('StoreDoubleParameter','f_r_0',f_r_0);
mws.invoke('StoreDoubleParameter','f_r_1',f_r_1);
mws.invoke('StoreDoubleParameter','f_r_2',f_r_2);
mws.invoke('StoreDoubleParameter','f_r_3',f_r_3);
mws.invoke('StoreDoubleParameter','f_r_4',f_r_4);
mws.invoke('StoreDoubleParameter','f_x_0',f_x_0);
mws.invoke('StoreDoubleParameter','f_x_1',f_x_1);
mws.invoke('StoreDoubleParameter','f_x_2',f_x_2);
mws.invoke('StoreDoubleParameter','f_x_3',f_x_3);
mws.invoke('StoreDoubleParameter','f_x_4',f_x_4);
mws.invoke('StoreDoubleParameter','f_y_0',f_y_0);
mws.invoke('StoreDoubleParameter','f_y_1',f_y_1);
mws.invoke('StoreDoubleParameter','f_y_2',f_y_2);
mws.invoke('StoreDoubleParameter','f_y_3',f_y_3);
mws.invoke('StoreDoubleParameter','f_y_4',f_y_4);

% Update the model before solving
mws.invoke('RebuildOnParametricChange',false,false);

%% Run CST simulation

% Time domain solver 'Solver'
% Freq domain 'FDSolver'
% Eigenmode 'EigenmodeSolver'
solver = invoke(mws,'FDSolver');

% Start solver
invoke(solver, 'start');

% save file
% invoke(mws, 'save')

% close file
% invoke(cst,'quit');

%% Retrieve S-parameters

resultTree = mws.invoke('Resulttree');

sparamname = {'S1,1';'S2,1';'S1,2';'S2,2'};
sparam = [];
for ii = 1:4
result1D = resultTree.invoke('GetResultFromTreeItem',['1D Results\S-Parameters\',sparamname{ii}],'3D:RunID:0');
freq = result1D.invoke('GetArray','x');
s_real = result1D.invoke('GetArray','yre');
s_im = result1D.invoke('GetArray','yim');
sparam(ii,:) = s_real + 1i*s_im;
end

% Close CST model
invoke(mws,'quit');

% Plot the s-parameters
figure(1); clf;
hold on
for ii = 1:4
plot(freq,20*log10(abs(sparam(ii,:))),'DisplayName',sparamname{ii})
end
xlabel('Frequency (GHz)')
ylabel('S-parameter (dB)')
legend('Location','SouthWest')

%% Create Cost function 

% Creating a cost function can be a challenge and depends on what you want
% to achieve. I'm going to try and optimize for a passband from 61 to 63
% GHz. I'm not really sure what is the best cost function or what we want
% to achieve. This is just a first attempt.

% Find the reflection and transmission coefficients
S11 = (abs(sparam(1,:))).^2;
S21 = (abs(sparam(2,:))).^2;
% find the indices of the passband
passband_lower_freq = 60;
passband_upper_freq = 62;
passbandfreq = find((freq>=passband_lower_freq)&(freq<=passband_upper_freq));

% Find the indices of where we don't want the signal to pass
transition_width = 1; 
stop_freq = find((freq < passband_lower_freq-transition_width) |...
(freq > passband_upper_freq+transition_width));

% CMAES or GA or PSO in MATLAB are all set up to minimize a function.
% Therefore, since I'm trying to maximize the transmission from 61 to 63, I
% will try subtracting the worst performing S21(linear) from 1 and try to
% minimize this difference.
passband = 1-mean(S21(passbandfreq));
middle_passband = 1- S21(62);

% Here we will find the worst stopband performance.  Here we are trying
% minimize the transmision.
stopband = mean(S21(stop_freq));

stop_band_s11 = mean(S11(stop_freq));
stop_band_s11_middle = S11(62)

OBJ = passband + 2 * stopband  + stop_band_s11;

savefig(string(OBJ) + '.fig')

OBJ
%% Save results

% It's nice to save the results from the optimization. This allows one to
% check on the optimization progress as it runs.
fid = fopen('Results.txt','a+');
fprintf(fid,'%0.6f\t',numel(vars),vars(:).',numel(OBJ),OBJ(:));
fprintf(fid,'\n');
fclose(fid);

pause(5);





