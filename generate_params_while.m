function [r_0,r_1, r_2, r_3, r_4, x_0, x_1, x_2, x_3, x_4, y_0, y_1, y_2, y_3, y_4, diel] = generate_params_while(Max_radius, Min_radius, Max_x, Min_x, Max_y, Min_y, Min_diel, Max_diel)
    is_crossed = 1;
    while is_crossed == 1
        [r_0, r_1, r_2, r_3, r_4, x_0, x_1, x_2, x_3, x_4, y_0, y_1, y_2, y_3, y_4, diel] = generate_params(Max_radius, Min_radius, Max_x, Min_x, Max_y, Min_y, Min_diel, Max_diel);
        is_crossed_0 = check(r_0, r_1, x_0, x_1, y_0, y_1);
        is_crossed_1 = check(r_0, r_2, x_0, x_2, y_0, y_2);
        is_crossed_2 = check(r_0, r_3, x_0, x_3, y_0, y_3);
        is_crossed_3 = check(r_0, r_4, x_0, x_4, y_0, y_4);
        is_crossed_4 = check(r_1, r_2, x_1, x_2, y_1, y_2);
        is_crossed_5 = check(r_1, r_3, x_1, x_3, y_1, y_3);
        is_crossed_6 = check(r_1, r_4, x_1, x_4, y_1, y_4);
        is_crossed_7 = check(r_2, r_3, x_2, x_3, y_2, y_3);
        is_crossed_8 = check(r_2, r_4, x_2, x_4, y_2, y_4);
        is_crossed_9 = check(r_3, r_4, x_3, x_4, y_3, y_4);
      
        if (is_crossed_0 == 0) & (is_crossed_1 == 0) & (is_crossed_2 == 0) & (is_crossed_3 == 0) & (is_crossed_4 == 0) & (is_crossed_5 == 0) & (is_crossed_6 == 0) & (is_crossed_7 == 0) & (is_crossed_8 == 0) & (is_crossed_9 == 0)
            is_crossed = 0;
        end
        
    end
        
            
end

