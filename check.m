function is_crossed = check(r_0, r_1, x_0, x_1, y_0, y_1)
    distance = sqrt((x_0 - x_1)^2 + (y_0 - y_1)^2)
    if distance >= (r_0 + r_1)
        is_crossed = 0
    else
        is_crossed = 1
    end
end
