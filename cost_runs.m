clc; clear all; close all;

%%

% Number of variables 
varargin.genes = 10; % 3 radii, 2 offset distances

%%
bits_total = varargin.genes;

opts = struct();
% Lower and Upper bands
opts.LBounds = [0.05; 0.05; 0.05; 11.76; 15.68; 0; -1.96; -1.96; -1.96; 6] ;
% opts.UBounds = 3*ones(varargin.genes,1);
opts.UBounds = [1.5876; 1.5876; 1.5876; 15.68; 25.48; 11.76; 1.96; 1.96; 1.96; 11];
opts.DispModulo=10;
% opts.PopSize = 12;
opts.StopOnStagnation = 'on';
opts.StopOnWarnings = 0;
% opts.MaxFunEval = 1200;
opts.StopFunEvals = 300*(varargin.genes)^2;
opts.Restarts = 10;
opts.Tolx = 1E-2; %%% This controls when the optimizer will restart. A smaller number will mean the optimizer will run longer before restarting.
opts.CMA.active = 2;
opts.Noise.on = 0;

t = 'Opt';
opts.SaveFilename = ['optimal_' t '.mat'];
opts.EvalInitialX = 'yes';
opts.EvalParallel = 'no';
opts.LogFilenamePrefix = ['outcmaes_' t '_'];
if(exist(opts.SaveFilename)==2)
    opts.Resume = 'yes';
else
    opts.Resume = 'no';
end

% xstart is the initial starting point
xstart = opts. LBounds + rand(bits_total,1).*(opts.UBounds-opts.LBounds);
% xstart = [0.341;...
%     0.341;...
%     0.341;...
%     13.72;...
%     15.68;...
%     11.76;...
%     0;...
%     0;...
%     0];

xstart = [0.5;	0.5;	0.5;	13.7;	15.6;	11.8;	0;	0;	0;	8.6];

%% This is the code that runs cmaes

[xmin, ...      % minimum search point of last iteration
    fmin, ...      % function value of xmin
    counteval, ... % number of function evaluations done
    stopflag, ...  % stop criterion reached
    out, ...       % struct with various histories and solutions
    bestever ...   % struct containing overall best solution (for convenience)
    ] = cmaes('cost',xstart,(opts.UBounds-opts.LBounds)/70,opts);
    
